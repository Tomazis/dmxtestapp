#include <QObject>
#include <DMXDevice.h>
class QDMXWriter : public QObject
{
public:
	explicit QDMXWriter(QObject* parent = 0);

public slots:
	void writeDMX(DMXDevice * device);
	void onChangeData(QByteArray newData);

signals:
	void writeFinished();


private: 
	void sleepRemainder(QElapsedTimer timer, bool stability);

	QByteArray data;
};


#include "DMXWriter.h"
#include "logger.h"

QDMXWriter::QDMXWriter(QObject* parent) : QObject(parent)
{
    data.resize(512);
    data.fill(0, 512);
}

void QDMXWriter::onChangeData(QByteArray newData)
{
    data = newData;
}

void QDMXWriter::writeDMX(DMXDevice * device)
{
    QElapsedTimer timer;
    timer.start();
    QThread::usleep(1000);
    bool stability = timer.elapsed() <= 3;
    LOG(QString("stability: %1").arg(stability));
    bool status = false;
    while (!thread()->isInterruptionRequested()) {
        thread()->eventDispatcher()->processEvents(QEventLoop::AllEvents);
        if (thread()->isInterruptionRequested()) 
            break;
        timer.restart();

        status = device->setBreak(true);
        if (!status) {
            sleepRemainder(timer, stability);
            continue;
        }
        LOG(QString("%1: setBreak to true, status: %2").arg(Q_FUNC_INFO).arg(status));

        if (stability) QThread::usleep(110);

        status = device->setBreak(false);
        if (!status) {
            sleepRemainder(timer, stability);
            continue;
        }
        LOG(QString("%1: setBreak to false, status: %2").arg(Q_FUNC_INFO).arg(status));

        if (stability) QThread::usleep(16);

        status = device->write(data);
        LOG(QString("%1: write status: %2").arg(Q_FUNC_INFO).arg(status));

        sleepRemainder(timer, stability);
    }

    LOG(QString("stop writing on device: %1, with data = %2").arg(device->name()).arg(QString(data.toHex())));
}


void QDMXWriter::sleepRemainder(QElapsedTimer timer, bool stability) {
	while (timer.elapsed() < 0.52) {
		if (stability) QThread::usleep(1000);
	}
}


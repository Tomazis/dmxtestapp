#include "DMXPlugin.h"
#include "DMXDevice.h"
#include "logger.h"

QDmxUsbPlugin::QDmxUsbPlugin(QObject *parent): QObject(parent)
{
    m_writer = new QDMXWriter();
    m_writer->moveToThread(&writerThread);
    connect(this, &QDmxUsbPlugin::startWrite, m_writer, &QDMXWriter::writeDMX);
    connect(this, &QDmxUsbPlugin::changeWritingData, m_writer, &QDMXWriter::onChangeData);
}

QDmxUsbPlugin::~QDmxUsbPlugin()
{
}

QString QDmxUsbPlugin::errorString()
{
    QString r = m_lastError;
    m_lastError.clear();
    return r;
}

bool QDmxUsbPlugin::outputIsOpened(quint32 device, quint32 port) const
{
    bool result = m_openedOutput.contains(device, port);
    LOG(QString("%1: device = %2, port = %3, result = %4").arg(Q_FUNC_INFO).arg(device).arg(port).arg(result));
    return result;
}

void QDmxUsbPlugin::init()
{
    LOG(QString("%1: rescanDevices started").arg(Q_FUNC_INFO));
    rescanDevices();
    emit rescanDevicesFinished();
    LOG(QString("%1: rescanDevices finished").arg(Q_FUNC_INFO));
}

void QDmxUsbPlugin::rescanDevices()
{
    qDeleteAll(m_deviceList);
    m_deviceList.clear();
    LOG(QString("%1: _deviceList cleared").arg(Q_FUNC_INFO));

    QList<DMXDevice*> deviceList;

    deviceList << DMXDevice::devices(deviceList);

    int i = 0;
    //for each found device
    foreach (DMXDevice* iface, deviceList)
    {
        m_deviceList[i++] = reinterpret_cast<DMXDevice*>(iface);
        LOG(QString("%1: %2 %3 %4 %5").arg(Q_FUNC_INFO).arg(i).arg(iface->name()).arg(iface->vendorID()).arg(iface->productID()));
    }
    LOG(QString("%1: _deviceList updated, size = %2").arg(Q_FUNC_INFO).arg(m_deviceList.count()));
}


bool QDmxUsbPlugin::openOutput(quint32 device, quint32 port)
{
    bool opened = false;
    LOG(QString("%1: device: %2").arg(Q_FUNC_INFO).arg(device));

    if (device < quint32(m_deviceList.size()))
    {
        m_openedOutput.insert(device, port);
        m_deviceList[device]->open();
        m_deviceList[device]->setBaudRate();
        m_deviceList[device]->setLineProperties();
        m_deviceList[device]->setBreak(false);
        LOG(QString("%1: device %2, port %3 added to _openedOutput").arg(Q_FUNC_INFO).arg(device).arg(port));
    }
    opened = outputIsOpened(device, port);
    LOG(QString("%1: output on device %2, port %3 is opened: %4").arg(Q_FUNC_INFO).arg(device).arg(port).arg(opened));
    return opened;
}

bool QDmxUsbPlugin::closeOutput(quint32 device, quint32 port)
{
    bool closed = false;
    LOG(QString("%1: output %2 opened on device %3: %4").arg(Q_FUNC_INFO).arg(port).arg(device).arg(closed));
    if(device >= quint32(m_deviceList.size())) {
        LOG(QString("%1: no such device (%2)").arg(Q_FUNC_INFO).arg(device));
        return false;
    }
    closed = m_deviceList[device]->close();
    if(closed) {
        LOG(QString("%1: output %2 closed").arg(Q_FUNC_INFO).arg(device));
    } else {
        LOG(QString("%1: unable to close output %2").arg(Q_FUNC_INFO).arg(device));
    }
    return closed;
}

void QDmxUsbPlugin::writeDmx(quint32 device, quint32 port, QByteArray data)
{
    LOG(QString("%1: device = %2, port = %3, data = %4").arg(Q_FUNC_INFO).arg(device).arg(port).arg(QString(data.toHex())));
    if (device >= quint32(m_deviceList.size())) {
        LOG(QString("%1: no such device (%2)").arg(Q_FUNC_INFO).arg(device));
        return;
    }
    
    writerThread.start();
    emit changeWritingData(data);
    emit startWrite(m_deviceList[device]);
}

void QDmxUsbPlugin::onStopWrite()
{
    writerThread.requestInterruption();
    writerThread.exit();
}

void QDmxUsbPlugin::onChangeWriteData()
{
    QByteArray data;
    data.resize(512);
    data.fill('a', 512);
    emit changeWritingData(data);
}


import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQuick.Dialogs 1.2

ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("DMX test application")

    ColumnLayout {
        anchors {
            fill: parent
            margins: 5
        }
        RowLayout {
            Button {
                text: qsTr("Init")
                onClicked: {
                    project.onInit()
                }
            }
            Label {
                text: project.initStatus
            }
        }
        RowLayout {
            TextField {
                id: deviceNumberTextEdit
                validator: IntValidator {
                    bottom: 0
                    top: 100
                }
            }
            Button {
                text: qsTr("Open output")
                onClicked: {
                    project.onOpenOutput(deviceNumberTextEdit.text)
                }
            }
            Label {
                text: project.openOutputStatus
            }
        }
        RowLayout {
            Button {
                text: qsTr("Send single DMX")
                onClicked: {
                    project.onSendDmxPkg(deviceNumberTextEdit.text)
                }
            }
            Button {
                text: qsTr("Begin sending DMX")
                onClicked: {
                    project.onBeginSendDmxPkg(deviceNumberTextEdit.text)
                }
            }
            Button {
                text: qsTr("End sending DMX")
                onClicked: {
                    project.onEndSendDmxPkg()
                }
            }
            Button {
                text: qsTr("Change DMX")
                onClicked: {
                    project.onChangeDmxPkg()
                }
            }
        }
        RowLayout {
            Button {
                text: qsTr("Close output")
                onClicked: {
                    project.onCloseOutput(deviceNumberTextEdit.text)
                }
            }
            Label {
                text: project.closeOutputStatus
            }
        }
        Flickable {
            Layout.fillWidth: true
            Layout.fillHeight: true
            interactive: false
            TextArea.flickable: TextArea {
                readOnly: true
                text: project.logString
                wrapMode: TextArea.Wrap
            }
            ScrollBar.vertical: ScrollBar {
                anchors {
                    right: parent.right
                }
                policy: ScrollBar.AlwaysOn
            }
        }
        RowLayout {
            Button {
                text: qsTr("Clear")
                onClicked: {
                    project.clearLog();
                }
            }
            Button {
                text: qsTr("Save")
                onClicked: {
                    saveDialog.open()
                }
            }
        }
    }
    FileDialog {
        id: saveDialog
        selectExisting: false
        selectMultiple: false
        nameFilters: [ "Text files (*.txt)" ]
        onAccepted: {
            project.saveLog(fileUrl);
        }
    }
}
